package com.company.Interfaces;


import com.company.commands.AbstractCommand;
import com.company.entity.User;

import java.util.List;

public interface ServiceLocator {


Service getService(String name);
void setUser(User user);
User getUser();
    List<AbstractCommand> getCommands();


}
