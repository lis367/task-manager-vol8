package com.company.Interfaces;

import com.company.entity.User;
import com.company.exception.ObjectIsNotFound;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public interface UserServiceInterface {

    void registration(String login, String password) throws Exception;
    void update(User user) throws ObjectIsNotFound, IOException;
    String generateMD5 (String password) throws NoSuchAlgorithmException;
    User read(String login, String password);
    void save (User user) throws Exception;
    void load() throws IOException;
    void remove(User user);
    void setUserRepository(UserRepoInterface userRepositoryImpl);

}
