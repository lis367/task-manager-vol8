package com.company.Interfaces;

import com.company.entity.Project;
import java.util.ArrayList;
import java.util.Map;

public interface ProjectRepoInterface {

    Project findOne(String id); //read
    ArrayList<Project> findAll(String id); //readAll
    void persist (Project project); // create
    void merge (Project project); //update
    void remove (String string); // delete
    void removeAll(); // deleteAll

}
