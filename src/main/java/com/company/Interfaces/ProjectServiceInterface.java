package com.company.Interfaces;

import com.company.entity.Project;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;

import java.util.ArrayList;
import java.util.Date;

public interface ProjectServiceInterface {


    String projectCreate(String name, Date dateStart, Date dateEnd, String userId);
    Project read(String id);
    void update(Project project);
    void projectRemove(String id) throws ObjectIsNotFound;
    ArrayList projectList();
    void projectClear();
    void setProjectRepository(ProjectRepoInterface projectRepository);
    void setTaskRepository(TaskRepoInterface taskRepository);
    void setBootstrap (Bootstrap bootstrap);


}
