package com.company.Interfaces;

import com.company.entity.Task;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;

import java.util.ArrayList;
import java.util.Date;

public interface TaskServiceInterface {

     String taskCreate(String name, String projectId, Date dateStart, Date dateEnd, String userId) throws Exception;
     Task read (String id);
     void update(Task task);
     void taskRemove(String id) throws ObjectIsNotFound;
     ArrayList taskList();
     void taskClear();
     void setTaskRepository(TaskRepoInterface taskRepository);
     void setProjectService(ProjectServiceInterface projectService);
     void setBootstrap(Bootstrap bootstrap);
}
