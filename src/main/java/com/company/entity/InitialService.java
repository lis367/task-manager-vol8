package com.company.entity;

import com.company.service.ProjectServiceImpl;
import com.company.service.TaskServiceImpl;
import com.company.service.TerminalServiceImpl;
import com.company.service.UserServiceImpl;

public final class InitialService {
    public Object lookup (String name){
        if (name.equalsIgnoreCase("Project-service")){
            return new ProjectServiceImpl();
        }
        else if (name.equalsIgnoreCase("Task-service")){
            return new TaskServiceImpl();
        }
        else if(name.equalsIgnoreCase("User-service")){
            return new UserServiceImpl();
        } else if(name.equalsIgnoreCase("Terminal-service")){
            return new TerminalServiceImpl();
        }
        return null;
    }

}
