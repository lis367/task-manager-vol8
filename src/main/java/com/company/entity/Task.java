package com.company.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
@Getter
@Setter

public final class Task {
    @NotNull
    String name;
    @NotNull
    String id;
    @NotNull
    String description;
    @NotNull
    Date dateBegin;
    @NotNull
    Date dateEnd;
    @NotNull
    String projectID;
    @NotNull
    String userId;


    public Task(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
    }

}
