package com.company.repository;


import com.company.Interfaces.UserRepoInterface;
import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public final class UserRepositoryImpl implements UserRepoInterface {
    final Map<String, User> userRepository;


    public void loadFromFile () throws IOException {
        FileReader fr = new FileReader("D:\\gitlab\\DB.txt");
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            String[] arg = line.split(":");
            String login = arg[0];
            String password = arg[1];
            String userType = arg[2];
            String userId = arg[3];
            User user = new User(login,password);
            user.setUserId(userId);
            UserRoleType userRoleType;
            if(userType.equalsIgnoreCase("ADMIN")){
                userRoleType = UserRoleType.ADMIN;
            }
            else {
                userRoleType = UserRoleType.USER;
            }
            user.setUserRoleType(userRoleType);
            userRepository.put(userId,user);
        }
        br.close();
        fr.close();
    }

    public User findOne(String login, String password){
        for (Map.Entry <String, User> userEntry: userRepository.entrySet()){
            if(userEntry.getValue().getName().equals(login)&&userEntry.getValue().getPassword().equals(password)){
                return userEntry.getValue();
            }
        }
            return null;
    }

    public void persist(User user)
    {
        User userPersist = userRepository.get(user.getUserId());
        if (userPersist ==null){
            userRepository.put(user.getUserId(),user);
            try {
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    public void merge(User user) {
        User userUpdate = userRepository.get(user.getUserId());
        if(userUpdate ==null){
            try {
                throw new ObjectIsNotFound();
            } catch (ObjectIsNotFound objectIsNotFound) {
                objectIsNotFound.printStackTrace();
            }
        }
        else {
            userRepository.put(user.getUserId(), user);
        }
    }

    @Override
    public void remove(User user) {
        Iterator<Map.Entry<String, User>> iterator = userRepository.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, User> entry = iterator.next();
            if (user.getUserId().equals(entry.getValue().getUserId())) {
                iterator.remove();
            }
        }

        try {
            save();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save() throws IOException {
        FileWriter fw = new FileWriter("D:\\gitlab\\DB.txt");
        for (Map.Entry <String, User> userEntry: userRepository.entrySet()){
            fw.write(userEntry.getValue().getName()+":"+userEntry.getValue().getPassword()+":"+userEntry.getValue().getUserRoleType()+":"+userEntry.getValue().getUserId()+"\n");
            }
        fw.close();
        }

    public UserRepositoryImpl(Map<String, User> userRepository) {
        this.userRepository = userRepository;
    }


}
