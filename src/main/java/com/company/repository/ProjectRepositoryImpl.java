package com.company.repository;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Map;
@RequiredArgsConstructor

public final class ProjectRepositoryImpl implements ProjectRepoInterface  {

    final Map<String, Project> projectRepository;
    final TaskRepoInterface taskRepository;


    public Project findOne (String id) {
        return projectRepository.get(id);
    }

    public ArrayList<Project> findAll(String userID){
        ArrayList<Project> projects = new ArrayList<>();
        for (Map.Entry<String, Project> projectPool: projectRepository.entrySet()){
            if((projectPool.getValue().getUserId()).equals(userID)){
                projects.add(projectPool.getValue());
            }
        }
        return projects;
    }


    public void persist(Project project)
    {
        Project projectPersist = projectRepository.get(project.getId());
        if (projectPersist==null){
            projectRepository.put(project.getId(), project);
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }


        /* Проверка по совпадению имени?
        Project projectPersist = projectRepository.get(project.getId());
        for (Map.Entry<String,Project> projectEntry: projectRepository.entrySet()){
            if(projectEntry.getValue().getName().equals(project.getName())){
                try {
                    throw new ObjectIsAlreadyExist();
                } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
                    objectIsAlreadyExist.printStackTrace();
                }
            }
        }
        projectRepository.put(project.getId(),project);

         */
    }

    public void merge (Project project)
    {
        projectRepository.put(project.getId(),project);
    }

    public void remove(String id){
        projectRepository.remove(id);
        ArrayList<Task> taskWithProject = taskRepository.getTasksFromProject(id);
        for (int i=0;i<taskWithProject.size();i++){
            taskRepository.remove(taskWithProject.get(i).getId());
        }

    }

    public void removeAll(){
        projectRepository.clear();
        taskRepository.removeAll();
    }








}
