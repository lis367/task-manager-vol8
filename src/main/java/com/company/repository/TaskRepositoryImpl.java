package com.company.repository;

import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Map;
@RequiredArgsConstructor

public final class TaskRepositoryImpl implements TaskRepoInterface {


    final private Map<String, Task> taskRepository;

    public void persist(Task task)  {
        Task taskPersist = taskRepository.get(task.getId());
        if (taskPersist ==null){
            taskRepository.put(task.getId(), task);
        }
        else try {
            throw new ObjectIsAlreadyExist();
        } catch (ObjectIsAlreadyExist objectIsAlreadyExist) {
            objectIsAlreadyExist.printStackTrace();
        }
    }

    public Task findOne(String id){
        return taskRepository.get(id);
    }

    public ArrayList findAll(String userID){
        ArrayList<Task> tasks = new ArrayList<>();
        for (Map.Entry<String, Task> taskPool: taskRepository.entrySet()){
            if((taskPool.getValue().getUserId()).equals(userID)){
                tasks.add(taskPool.getValue());
            }
        }
        return tasks;
    }

    public void merge (Task task)
    {
        taskRepository.put(task.getId(), task);
    }

    public void remove(String id){
        taskRepository.remove(id);
    }

    public void removeAll(){
        taskRepository.clear();
    }


    public ArrayList getTasksFromProject(String projectID){
        ArrayList<Task> arrayList = new ArrayList<Task>();
        for (Map.Entry<String, Task> taskEntry : taskRepository.entrySet()) {
           if(projectID.equals(taskEntry.getValue().getProjectID())){
               arrayList.add(taskEntry.getValue());
            }
        }
        return arrayList;
    }


}

