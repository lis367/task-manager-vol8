package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserAuthorizationCommand extends AbstractCommand  {

    public String command() {
        return "user-authorization";
    }


    public String description() {
        return "Authorization";
    }


    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");
        System.out.println("ENTER LOGIN");
        String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userServiceImpl.generateMD5(terminalService.nextLine());
        if((userServiceImpl.read(login,password))==null){
            System.out.println("USER NOT FOUND");
            System.out.println("Registration:");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
            System.out.println("ENTER PASSWORD");
            password = userServiceImpl.generateMD5(terminalService.nextLine());
            userServiceImpl.registration(login,password);
        }
        else {
            System.out.println("Authorization success");
            serviceLocator.setUser(userServiceImpl.read(login,password));
        }

    }
    public boolean secureCommand() {
        return false;
    }


    public UserAuthorizationCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }

}
