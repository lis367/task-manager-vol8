package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserLoadFromDBCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-load";
    }

    @Override
    public String description() {
        return "Load Users from file in Repository";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        userServiceImpl.load();
        System.out.println("База успешно загружена");
    }
    public boolean secureCommand() {
        return false;
    }

    public UserLoadFromDBCommand(ServiceLocator serviceLocator) {
        setServiceLocator(serviceLocator);
    }

}
