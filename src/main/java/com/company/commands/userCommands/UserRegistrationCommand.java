package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@NoArgsConstructor

public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-registration";
    }

    @Override
    public String description() {
        return "Registration";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");

        System.out.println("ENTER LOGIN");
        final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = userServiceImpl.generateMD5(terminalService.nextLine());
        final User user = new User(login,password);
        final String id =  UUID.randomUUID().toString();
        user.setUserId(id);
        user.setUserRoleType(UserRoleType.USER);
        userServiceImpl.save(user);
        System.out.println("User id = "+ id + " with login "+login +" password "+password);
    }

    public boolean secureCommand() {
        return false;
    }

    public UserRegistrationCommand(ServiceLocator serviceLocator) {
        setServiceLocator(serviceLocator);
    }


}
