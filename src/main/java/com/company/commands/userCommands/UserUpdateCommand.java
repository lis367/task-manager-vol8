package com.company.commands.userCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.UserServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update User";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final UserServiceInterface userServiceImpl = (UserServiceInterface) serviceLocator.getService("User-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");

        System.out.println("ENTER NEW LOGIN");
        final String login = terminalService.nextLine();
        System.out.println("ENTER NEW PASSWORD");
        final String password = userServiceImpl.generateMD5(terminalService.nextLine());
        final User user = serviceLocator.getUser();
        user.setName(login);
        user.setPassword(password);
        serviceLocator.setUser(user);
        userServiceImpl.update(user);

    }

    public boolean secureCommand() {
        return true;
    }

    public UserUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
