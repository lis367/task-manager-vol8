package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.util.Scanner;
@NoArgsConstructor

public final class TaskViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");

        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try{
            if (taskServiceImpl.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
                System.out.println("Task name "+ taskServiceImpl.read(line).getName());
                System.out.println("Task begins in "+ taskServiceImpl.read(line).getDateBegin());
                System.out.println("Task ends in "+ taskServiceImpl.read(line).getDateEnd());
            }
            else {
                System.out.println("no rights to update task of other users");
            }
        }
        catch (NullPointerException npe){
            System.out.println("Task is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskViewCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
