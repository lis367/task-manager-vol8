package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");

        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        taskServiceImpl.taskClear();
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public TaskClearCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }

}
