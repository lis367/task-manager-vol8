package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
@NoArgsConstructor


public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");

        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        try {
            if(taskServiceImpl.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
            taskServiceImpl.taskRemove(line);
            System.out.println("TASK REMOVED");}
            else {
                System.out.println("no rights to delete task of other users");
            }
        }
        catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
        }


    }
    public boolean secureCommand() {
        return true;
    }

    public TaskRemoveCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }


}
