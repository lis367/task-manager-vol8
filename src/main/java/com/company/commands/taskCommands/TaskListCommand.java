package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
@NoArgsConstructor

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");

        System.out.println("[TASK LIST]");
        ArrayList<Task> tasksPool = taskServiceImpl.taskList();
        for(int i=0;i<tasksPool.size();i++){
            System.out.println("Task Name: "+tasksPool.get(i).getName() + " Task ID: " + tasksPool.get(i).getId() + " Project :"+tasksPool.get(i).getProjectID());
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public TaskListCommand(ServiceLocator bootstrap) { setServiceLocator(bootstrap);
    }

}
