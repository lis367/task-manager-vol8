package com.company.commands.taskCommands;

import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;

import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@NoArgsConstructor


public final class TaskCreateCommand extends AbstractCommand {


    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-service");
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String line = terminalService.nextLine().trim();
        if(line.isEmpty()){
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        final String projectID = terminalService.nextLine().trim();
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        final String dateStart = terminalService.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        String dateEnd = terminalService.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try{
            System.out.println("TASK ID IS");
            System.out.println(taskServiceImpl.taskCreate(line , projectID, dateFormatter.parse(dateStart) , dateFormatter.parse(dateEnd), serviceLocator.getUser().getUserId()) );
        }
        catch (ParseException e){
            System.out.println("Wrong format dd.mm.yyyy");
        }
        catch (ObjectIsNotFound e){
            System.out.println("Такого проекта не сущесвует");
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public TaskCreateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
