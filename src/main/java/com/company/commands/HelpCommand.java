package com.company.commands;


import com.company.Interfaces.ServiceLocator;
import lombok.NoArgsConstructor;

import java.util.List;
@NoArgsConstructor

public final class HelpCommand extends AbstractCommand {


    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public boolean secureCommand() {
        return false;
    }

    @Override
    public void execute() {
        List<AbstractCommand> commands = serviceLocator.getCommands();
        if (serviceLocator.getUser()==null) {
            for (int i = 0; i < commands.size(); i++) {
                if (commands.get(i).secureCommand() ==false){
                    System.out.println(commands.get(i).command() + ": " + commands.get(i).description());}
            }
        } else {
            for (int i = 0; i < commands.size(); i++) {
                System.out.println(commands.get(i).command() + ": " + commands.get(i).description());
            }

        }
    }

    public HelpCommand(ServiceLocator bootstrap) {
            setServiceLocator(bootstrap);
        }

}



