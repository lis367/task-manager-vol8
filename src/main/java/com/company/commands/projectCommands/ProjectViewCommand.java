package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import java.util.Scanner;
@NoArgsConstructor

public class ProjectViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        try{
            if (projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
                System.out.println("Project name "+ projectService.read(line).getName());
                System.out.println("Project begins in "+ projectService.read(line).getDateBegin());
                System.out.println("Project ends in "+ projectService.read(line).getDateEnd());
            }
            else {
                System.out.println("no rights to update projects of other users");
            }
        }
        catch (NullPointerException npe){
            System.out.println("Project is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectViewCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
