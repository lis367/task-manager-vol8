package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor


public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-Service");
        System.out.println("ENTER ID");

        String line = terminalService.nextLine();
        try {
            if(projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())){
            projectService.projectRemove(line);
                System.out.println("PROJECT&TASK REMOVED");

            }
            else{
                System.out.println("no rights to delete project of other users");
            }
        } catch (ObjectIsNotFound objectIsNotFound) {
            System.out.println("PROJECT IS NOT FOUND");
        }


    }
    public boolean secureCommand() {
        return true;
    }

    public ProjectRemoveCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);

    }

}
