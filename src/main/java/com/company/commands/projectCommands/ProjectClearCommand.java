package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.Interfaces.TaskServiceInterface;
import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor

public final class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        @NotNull
        final TaskServiceInterface taskServiceImpl = (TaskServiceInterface) serviceLocator.getService("Task-service");
        if (serviceLocator.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        projectService.projectClear();
        taskServiceImpl.taskClear();
        System.out.println("ALL PROJECT&TASK REMOVED");}
        else {
            System.out.println("ADMIN ROLETYPE ARE REQUIRED");
        }
    }


    public ProjectClearCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
    public boolean secureCommand() {
        return true;
    }




}
