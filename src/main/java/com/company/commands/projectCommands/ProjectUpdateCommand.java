package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.service.TerminalServiceImpl;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;


import java.text.SimpleDateFormat;
import java.util.Scanner;
@NoArgsConstructor

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-Service");
        final TerminalServiceImpl terminalService = (TerminalServiceImpl) serviceLocator.getService("Terminal-service");
        System.out.println("ENTER ID");
        String line = terminalService.nextLine();
        try {
            if (projectService.read(line).getUserId().equals(serviceLocator.getUser().getUserId())) {
                final Project projectPool = projectService.read(line);
                System.out.println("ENTER NEW NAME");
                line = terminalService.nextLine();
                projectPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
                final String dateStart = terminalService.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE PROJECT");
                final String dateEnd = terminalService.nextLine().trim();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                projectPool.setDateBegin(dateFormatter.parse(dateStart));
                projectPool.setDateEnd(dateFormatter.parse(dateEnd));
                projectService.update(projectPool);
                System.out.println("SUCCESS");
            } else {
                System.out.println("no rights to update projects of other users");
            }
        }
        catch (NullPointerException e){
            System.out.println("Project is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectUpdateCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
