package com.company.commands.projectCommands;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.ServiceLocator;
import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@NoArgsConstructor

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() {
        @NotNull
        final ProjectServiceInterface projectService = (ProjectServiceInterface) serviceLocator.getService("Project-service");

        System.out.println("[PROJECT LIST]");
        ArrayList <Project> projectPool = projectService.projectList();
        for(int i=0; i<projectPool.size();i++){
            System.out.println("Project Name: "+projectPool.get(i).getName() + " Project ID: " + projectPool.get(i).getId());
         }

    }
    public boolean secureCommand() {
        return true;
    }


    public ProjectListCommand(ServiceLocator bootstrap) {
        setServiceLocator(bootstrap);
    }
}
