package com.company.service;

import com.company.Interfaces.ProjectRepoInterface;
import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.TaskRepoInterface;
import com.company.entity.Project;
import com.company.Interfaces.Service;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public final class ProjectServiceImpl implements ProjectServiceInterface, Service {

    @NotNull
    private ProjectRepoInterface projectRepository;
    @NotNull
    private TaskRepoInterface taskRepository;
    @NotNull
    private Bootstrap bootstrap;

    public ProjectServiceImpl() {
    }

    public String projectCreate(final String name, final Date dateStart, final Date dateEnd, final String userId) {
        String id = UUID.randomUUID().toString();
        Project project = new Project(name, dateStart, dateEnd,id,  userId);
        projectRepository.persist(project);
        return project.getId();
    }

    public ArrayList projectList() {
        return projectRepository.findAll(bootstrap.getUser().getUserId());
    }

    public void projectClear() {
        projectRepository.removeAll();
    }

    public void projectRemove(final String id) throws ObjectIsNotFound {
        if(projectRepository.findOne(id)==null){
                throw new ObjectIsNotFound();
        }
        else {
            projectRepository.remove(id);
            ArrayList<String>copyofTask = taskRepository.getTasksFromProject(id);
            for(int i=0;i<copyofTask.size();i++){
                taskRepository.remove(copyofTask.get(i));
            }
        }
    }

    public Project read(final String id) {
        return projectRepository.findOne(id);
    }


    public void update(final Project project){
        projectRepository.merge(project);
    }

    public String getName() {
        return "Project-Service";
    }







}
