package com.company.service;

import com.company.Interfaces.ProjectServiceInterface;
import com.company.Interfaces.Service;
import com.company.Interfaces.TaskRepoInterface;
import com.company.Interfaces.TaskServiceInterface;
import com.company.entity.Task;
import com.company.exception.ObjectIsNotFound;
import com.company.util.Bootstrap;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public final class TaskServiceImpl implements TaskServiceInterface, Service {

    @NotNull
    private TaskRepoInterface taskRepository;
    @NotNull
    private ProjectServiceInterface projectService;
    @NotNull
    private Bootstrap bootstrap;


    public String taskCreate(String name, String projectId, Date dateStart, Date dateEnd, String userId) throws Exception {
        if (projectService.read(projectId)==null){
           throw new ObjectIsNotFound();
        }
        if(projectService.read(projectId).getUserId()!=bootstrap.getUser().getUserId()){
            System.out.println("You can't create task with projects of others users");
            throw new ObjectIsNotFound();
        }
        String id = UUID.randomUUID().toString();
        Task task = new Task(name, id, userId);
        task.setDateBegin(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectID(projectId);
        taskRepository.persist(task);
        return task.getId();
    }

    public Task read (String id){
        return taskRepository.findOne(id);
    }

    public void update(Task task){
        taskRepository.merge(task);
    }

    public void taskRemove(String id) throws ObjectIsNotFound {
        if(taskRepository.findOne(id)==null){
                throw new ObjectIsNotFound();
        }
        else {
            taskRepository.remove(id);
        }
    }

    public ArrayList taskList() {
        return taskRepository.findAll(bootstrap.getUser().getUserId());
    }

    public void taskClear() {
        taskRepository.removeAll();
    }


    @Override
    public String getName() {
        return "Task-service";
    }
}
